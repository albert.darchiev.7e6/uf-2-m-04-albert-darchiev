/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.34
*/
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix el teu nom:")
    val nom = scanner.next()
    print("Bon dia $nom")
}
