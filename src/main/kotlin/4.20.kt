import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/17
* TITLE: 4.20
*/

fun main() {
    val scanner = Scanner(System.`in`)
    var hamming = 0

    println("ADN 1: ")
    val adn1 = scanner.next()
    println("ADN 2: ")
    val adn2 = scanner.next()
    val sameLength = adn1.length == adn2.length

    if (sameLength == false){
        println("Entrada NO valida")
        return
    }
    for (i in 0 until adn1.length){
        if (adn1[i] != adn2[i]) hamming++
    }
    println(hamming)


}