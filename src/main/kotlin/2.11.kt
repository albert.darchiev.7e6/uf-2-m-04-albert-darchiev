/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/06
* TITLE: 2.11 Calculadora
*/

import java.util.*
import kotlin.math.pow

fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce el 1r numero:")
    var num1 = scanner.nextInt()
    println("Introduce el 2o numero:")
    var num2 = scanner.nextInt()
    println("Que operacion quieres hacer? (+ | - | * | / | %)")
    var operacion = scanner.next()
    if (operacion == "+") println(num1 + num2)
    else if (operacion == "-") println(num1 - num2)
    else if (operacion == "*") println(num1 * num2)
    else if (operacion == "/") println(num1 / num2)
    else if (operacion == "%") println(num1 % num2)
    else println("ERROR")

}