/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/13
* TITLE: 3.6 Eleva’l
*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("First number:")
    var num1 = scanner.nextInt()
    println("Second number:")
    var num2 = scanner.nextInt()

    var pow = 1
    for (i in 1..num2) {
        pow = pow*num1
    }
    println("RESULT: $pow")
}