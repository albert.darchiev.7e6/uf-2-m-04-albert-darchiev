    /*
    * AUTHOR: Albert Darchiev
    * DATE: 2022/10/27
    * TITLE: 4.12 Ordena l’array (up)
    */

    fun main(args: Array<String>) {

        var size = args.size-1
        println(size)
        var small = "0"
        var big = "0"

        for (j in 0..size) {
            for (i in 0 until size) {
                if (args[i].toInt() < args[i + 1].toInt()) {
                    continue
                } else if (args[i].toInt() > args[i + 1].toInt()) {
                    big = args[i]
                    small = args[i + 1]
                    args[i] = small
                    args[i + 1] = big
                }
            }
        }
        for (arg in args) print ("$arg ")
    }