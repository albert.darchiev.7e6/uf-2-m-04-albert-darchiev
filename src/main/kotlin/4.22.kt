import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/17
* TITLE: 4.22
*/

fun main() {
    val scanner = Scanner(System.`in`)

    println("Introduce una palabra: ")
    val word = scanner.next()

    val length = word.length
    var impar = length %2 != 0
    val halfLength = if (impar) length-1 / 2
                            else length/2
    var polindrom = true

    for (i in 0 until halfLength){
        if (polindrom == false) {
            println("NO es polindrom")
            return
        }
        if (word[i] == word[length-1-i]) polindrom = true
        else if (word[i] != word[length-1-i]) polindrom = false
        }
    if (polindrom) println("SI es palindrom")
    else println("NO es palindrom")
}