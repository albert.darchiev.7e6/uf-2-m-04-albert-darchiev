import java.util.Scanner

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/17
* TITLE: 4.17 Són iguals? v2
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("PALABRA 1: ")
    val word1 = scanner.next().uppercase()
    println("PALABRA 2: ")
    val word2 = scanner.next().uppercase()

    var comprobador = true

    val lenght = word1.length
    for (i in 0 until  lenght){
        if (word1[i] != word2[i]) comprobador = false
    }
    if (comprobador == false) println("No son iguales")
    else println("Son iguales")
}