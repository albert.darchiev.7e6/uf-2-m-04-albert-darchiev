/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.29 Equacions de segon grau
*/
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main(){
    val scanner = Scanner(System.`in`)
    println("Entrada:")
    val a = scanner.nextDouble()

    println("Entrada:")
    val b = scanner.nextDouble()

    println("Entrada:")
    val c = scanner.nextDouble()

    println (((-b) - (sqrt ((b.pow(2.0) -4*a * c)))) / (2*a))
    println (((-b) + (sqrt ((b.pow(2.0) -4*a * c)))) / (2*a))

}
