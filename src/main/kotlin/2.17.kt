/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/08
* TITLE: 2.17 Puges o baixes?
*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("First number:")
    var num1 = scanner.nextInt()
    println("Second number:")
    var num2 = scanner.nextInt()
    println("Third number:")
    var num3 = scanner.nextInt()

    if((num1 > num2 && num2 > num3) || (num1 == num2 && num2 > num3) || (num1 > num2 && num2 == num3)) println("DESCENDENT")
        else if ((num1 < num2 && num2 < num3) || (num1 == num2 && num2 < num3) || (num1 < num2 && num2 == num3)) println("ASCENDENT")
    else println("CAP DE LES DUES")
}