/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 4.6 Mínim i màxim

*/

import java.util.*
fun main(args: Array<String>) {
    var low = args[0].toInt()
    var high = args[0].toInt()
    for (arg in args) {
        val argINT = arg.toInt()
        if (low > argINT) low = argINT
        else if (high < argINT) high = argINT
    }
    println("LOW: $low")
    println("HIGH: $high")
}