/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/08
* TITLE: 2.20 Conversor d’unitats
*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("First number:")
    var num1 = scanner.nextInt()
    println("Weight ( G | KG | TN ):")
    var weight = scanner.next()

    if (weight == "G"){
        println("KG: "+num1.toFloat()/1000)
        println("TN: "+num1.toFloat()/1000000)
    }
    else if (weight == "KG"){
        println("G: "+num1.toFloat()*1000)
        println("TN: "+num1.toFloat()/1000)
    }
    else if (weight == "TN"){
        println("G: "+num1.toFloat()*1000)
        println("KG: "+num1.toFloat()*1000000)
    }
    else println("ERROR")


}