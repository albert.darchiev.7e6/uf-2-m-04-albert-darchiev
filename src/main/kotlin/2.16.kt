/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/07
* TITLE: 2.16 Quants dies té el mes
*/

import java.util.*
import kotlin.math.pow

fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce el MES:")
    var month = scanner.nextInt()
    when (month){
        1,3,5,7,8,10,12 -> println(31)
        2-> println("28/29")
        4,6,9,11 -> println(30)
        !in 1..12 -> println("INCORRECTO")

    }

}