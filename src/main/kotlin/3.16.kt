import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.16 Coordenades en moviment
*/

fun main(){
    val scanner = Scanner(System.`in`)
    //n -1R  e +1R  s +1L
    var coordL = 0
    var coordR = 0
    //(O/E , S/N)

    do{
        var coord = scanner.next().single()
        when(coord){
            'n' -> coordR -= 1
            's' -> coordR += 1
            'e' -> coordL += 1
            'o' -> coordL -= 1
        }
    }while (coord != 'z')
    println("($coordL, $coordR)")
}