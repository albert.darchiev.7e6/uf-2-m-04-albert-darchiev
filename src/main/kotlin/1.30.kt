/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.30 Quant de temps?
*/
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main(){
    val scanner = Scanner(System.`in`)
    println("Entrada:")
    val a = scanner.nextInt()

    val hora = ((a / 60) / 60)


    print(hora)
    print(" horas  ")
    print(((a / 60) % 60))
    print(" minutos  ")
    print (a % 60)
    print(" segundos")
}
