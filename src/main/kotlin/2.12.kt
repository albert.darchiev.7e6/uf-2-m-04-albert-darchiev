/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/06
* TITLE: 2.12 Comprova la data
*/

import java.util.*
import kotlin.math.pow

fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce el DIA:")
    var dia = scanner.nextInt()
    println("Introduce el MES:")
    var mes = scanner.nextInt()
    println("Introduce el AÑO:")
    var year = scanner.nextInt()

    if (dia in 1..31 && mes == 1 || mes == 3 || mes == 5 || mes ==7 || mes ==8 || mes ==10 || mes ==12 && year in 2000..2022) println("La fecha es correcta")
        else if(dia in 1..30 && mes == 4 || mes == 6 || mes == 9 || mes ==11 && year in 2000..2022) println("La fecha es correcta")
        else if(dia in 1..28 && mes == 2 && year in 2000..2022) println("La fecha es correcta")
        else (print("INCORRECTO"))
}

