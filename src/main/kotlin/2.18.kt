/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/08
* TITLE: 2.18 Valor absolut
*/

import java.util.*
import kotlin.math.absoluteValue

fun main() {
    var scanner = Scanner(System.`in`)
    println("Type a number:")
    var num1 = scanner.nextInt()
    println(num1.absoluteValue)
}