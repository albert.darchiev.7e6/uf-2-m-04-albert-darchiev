import java.lang.Math.abs
import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.21 Triangle invertit d’*
*/

fun main() {
    val scanner = Scanner(System.`in`)
    var n = scanner.nextInt()
    for (i in 1 until 2*n){
        val espai = abs(n-i)
        val asterisc = if (2*i-1 < 2*n) 2*i-1
                        else (2*i-1)-(abs(n-i)*4)
        repeat(espai){print(" ")}
        repeat(asterisc){ print("*")}
        println()
    }
}