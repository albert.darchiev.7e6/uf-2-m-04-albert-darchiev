/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/14
* TITLE: 4.5 En quina posició?

*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce un numero:")
    var num1 = scanner.nextInt()
    val nums = arrayOf(1, 2, 3, 4, 5, 8, 21, 25, 28)

    if (num1 in nums) println(nums.indexOf(num1))
    else println("No esta contingut")
}

