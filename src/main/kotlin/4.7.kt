/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 4.7 Inverteix l’array
*/

import java.util.*
fun main(args: Array<String>) {
    //OPCION 1
    for (i in args.lastIndex downTo 0) {
        println(args[i])
    }

    //OPCION 2
    val InvertedArray = Array(args.size){"0"}
    for (i in 0..args.lastIndex){
        InvertedArray[i] = args[args.lastIndex-i]
    }
    for (a in InvertedArray) println(a)

}

