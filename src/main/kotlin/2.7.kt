/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/29
* TITLE: 2.7 Parell o senar?
*/

import java.util.*
import kotlin.math.pow

fun main() {
    val scanner = Scanner(System.`in`)
    println("num1:")
    val num1 = scanner.nextInt()

    if (num1 % 2 == 0){println("Parell")}
    else {println("Senar")
    }
}
