/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.9 Calcula el descompte
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("preu original:")
    val numero1 = scanner.nextDouble()

    println("preu actual:")
    val numero2 = scanner.nextDouble()

    print("El descompte es de: ")
    val opr1 = (numero1 - numero2)
    val opr2 = (opr1 / numero1)
    val opr3 = (opr2 * 100)
    print(opr3)
    print(" %")
}
