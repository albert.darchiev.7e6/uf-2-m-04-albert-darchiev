/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.33
*/
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main(){
    val scanner = Scanner(System.`in`)
    println("Entrada:")
    val a = scanner.nextInt()
    val b = scanner.nextInt()
    print(b % a == 0)
}
