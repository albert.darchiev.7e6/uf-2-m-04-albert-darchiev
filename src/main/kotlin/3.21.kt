import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.21 Triangle invertit d’*
*/

fun main() {
    val scanner = Scanner(System.`in`)
    var num1 = scanner.nextInt()
    var x = 0
    for (i in num1 downTo 1){
        print(" ".repeat(x))
        x += 1
        for (j in 1..i){
            print("*")
        }
        println("")
    }
}