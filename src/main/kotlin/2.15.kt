/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/07
* TITLE: 2.15 És vocal o consonant?
*/

import java.util.*

fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce una letra:")
    var letter = scanner.next()
    when(letter){
        "a","e","i","o","u","A","E","I","O","U" -> println("Es vocal")
        else -> println("No es vocal")

    }

}