/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/21
* TITLE: 4.9
*/

fun main(args: Array<String>) {
    for (i in 0..args.lastIndex) {
        for (j in i+1..args.lastIndex){
            if (args[i] == args[j]) println(args[i])
        }
    }
}