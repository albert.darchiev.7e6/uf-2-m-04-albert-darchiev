/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/14
* TITLE: 4.4 És contingut?

*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce un numero:")
    var num1 = scanner.nextInt()
    val nums = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    if (num1 in nums) println("Si esta contingut")
    else println("No esta contingut")
}

