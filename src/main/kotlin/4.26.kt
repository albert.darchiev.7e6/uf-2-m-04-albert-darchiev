import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/29
* TITLE: 4.26
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introduce un texto: ")
    val word = scanner.nextLine().toString()

    if ("hola" in word) println("Hola")
    else println("Adeu")
}
