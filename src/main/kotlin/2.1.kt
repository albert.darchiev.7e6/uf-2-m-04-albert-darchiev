/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/29
* TITLE: 2.1 Màxim de 3 nombres enters
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("num1:")
    val num1 = scanner.nextInt()
    println("num2:")
    val num2 = scanner.nextInt()
    println("num3:")
    val num3 = scanner.nextInt()

    print("El numero més alt es: ")
    if (num1 > num2 && num1 > num3){ println (num1)}
    else if (num2 > num1 && num2 > num3){ println (num2)}
    else println(num3)



}
