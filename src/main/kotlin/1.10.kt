/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.10 Quina és la mida de la meva pizza?
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("Diametro:")
    val numero1 = scanner.nextInt()

    print("El descompte es de: ")
    val radio = (numero1 / 2)
    println (Math.PI * Math.pow(radio.toDouble(), 2.0))
}
