/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.3 Suma de dos nombres enters
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)

    println("Introdueix un número:")
    val numero1 = scanner.nextInt()

    println("Introdueix un número:")
    val numero2 = scanner.nextInt()

    println("Aquesta és la suma dels 2 números introduits:")
    mult(numero1, numero2)
}

fun mult(numero1: Int, numero2: Int):Int{
    return numero1 * numero2
}