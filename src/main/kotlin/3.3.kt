/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/09
* TITLE: 3.3 Imprimeix el rang
*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("First number:")
    var num1 = scanner.nextInt()
    println("Second number:")
    var num2 = scanner.nextInt()

    if (num1 <= num2) {
        for (i in num1 until num2){
            print("$i, ")

        }
    print(num2)}
    else println("INVALID NUMBERS")
}