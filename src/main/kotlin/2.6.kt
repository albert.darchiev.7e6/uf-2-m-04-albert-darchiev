/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/29
* TITLE: 2.6 Quina pizza és més gran?
*/

import java.util.*
import kotlin.math.pow

fun main() {
    val scanner = Scanner(System.`in`)
    println("num1:")
    val num1 = scanner.nextDouble()
    println("num2:")
    val num2 = scanner.nextDouble()
    println("num3:")
    val num3 = scanner.nextDouble()
    
    val radio = num1/2
    val pizza_rodona = Math.PI * (radio.pow(2))
    val pizza_cuadrada = num2 * num3
    if (pizza_rodona > pizza_cuadrada){println(pizza_rodona)}
    else println(pizza_cuadrada)
}
