import java.util.Scanner

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/17
* TITLE: 4.18
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introduce un apalabra: ")
    var word = scanner.next()

    do {
        println("Introduce una letra: ")
        val letter = scanner.next()
            word = word.replace(letter, "")
    } while (letter != "0")
    println(word)
}