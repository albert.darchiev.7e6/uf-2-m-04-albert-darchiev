/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.23 És una lletra?
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Entrada:")
    val a1 = scanner.next().single()


    println ((a1 >= 'A') && (a1 <= 'Z') || (a1 >= 'a') && (a1 <= 'z'))
}
