import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/29
* TITLE: 4.25
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introduce un texto: ")
    val word = scanner.nextLine().toString()
    val list:List<String> = word.split("")

    for (i in list.size-1 downTo 0){
        print("${list[i]}")
    }
}
