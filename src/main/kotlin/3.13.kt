import java.util.Scanner

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.13 Logaritme natural de 2
*/

fun main(){
    var scanner = Scanner(System.`in`)
    println("Introduce un numero:")
    val num1 = scanner.nextInt()
    var result1 = -0.0
    var result2 = -0.0

    for (i in 1..num1 step 2){
        result1 += 1.toDouble() / i
    }

    for (i in 2..num1 step 2){
        result2 += 1.toDouble() / i
    }

    println("result1 = $result1")
    println("result2 = $result2")
    println(result1-result2)
}