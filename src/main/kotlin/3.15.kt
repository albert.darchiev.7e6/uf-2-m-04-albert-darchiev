import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.15 Endevina el número (ara amb intents)
*/

fun main(){
    val scanner = Scanner(System.`in`)
    var num1 = 111
    var random = (1..100).random()
    var vida = 6
    println("Introduce un numero:")

    while (num1 != random ){
        var num1 = scanner.nextInt()

        if (num1 !in 1..100) println("NUMERO INCORRECTE")
        else if (num1 == random) {
            println("HAS ENCERTAT!!")
            num1 = random
            return
        }
        else if (num1 < random){
            println("Massa baix")
            vida -= 1}
        else if (num1 > random) {
            println("Massa alt")
            vida -= 1
        }
        if (vida == 0) {
            println("HAS PERDUT")
            return
        }
    }
}