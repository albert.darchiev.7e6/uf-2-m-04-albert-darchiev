/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/09
* TITLE: 3.4 Imprimeix el rang 2
*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("First number:")
    var num1 = scanner.nextInt()
    println("Second number:")
    var num2 = scanner.nextInt()

    if (num1 < num2) {
        for (i in num1..num2) {
            print("$i, ")
        }}
    else if (num1 > num2) {
        for (i in num1 downTo num2){
            print("$i, ")
        }
    }

    else println("INVALID NUMBERS")
}