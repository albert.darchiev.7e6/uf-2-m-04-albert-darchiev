/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/26
* TITLE: 4.11
*/

import java.util.*
fun main(args: Array<String>) {
    var parell = 0
    var senar = 0
    for (i in 0 until args.size){
        if (args[i].toInt() % 2 == 0) parell ++
        else senar ++
    }
    println("Parells: $parell")
    println("Senars: $senar")
}